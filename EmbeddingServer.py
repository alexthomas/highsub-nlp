from flask import Flask, request, jsonify
from sentence_transformers import SentenceTransformer

model = SentenceTransformer('flax-sentence-embeddings/all_datasets_v4_MiniLM-L6')
app = Flask(__name__)


@app.route('/embedding', methods=['POST'])
def embedding():
    text = request.json['text']
    return jsonify(model.encode(text).tolist())


@app.route("/health", methods=['GET'])
def heath():
    return jsonify({'status': 'ok'})


if __name__ == '__main__':
    app.run(host='127.0.0.1')
