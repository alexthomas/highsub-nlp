FROM python:3.9
RUN mkdir "app"
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY *.py /app
CMD ["waitress-serve", "EmbeddingServer:app"]